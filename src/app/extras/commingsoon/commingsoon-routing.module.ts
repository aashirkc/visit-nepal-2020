import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { CommingsoonComponent } from './commingsoon.component';

const routes: Routes = [
  { path: '', component: CommingsoonComponent }
];
@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CommingsoonRoutingModule { }
