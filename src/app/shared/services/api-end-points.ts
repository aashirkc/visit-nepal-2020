export class ApiEndPoints {

  private static API_MAIN_ENDPOINT = 'http://localhost:8080/visitnepal';
  private static BASE_ENDPOINT = ApiEndPoints.API_MAIN_ENDPOINT  + '/rest/api/';


  public static SIGNUP_ENDPOINT = ApiEndPoints.API_MAIN_ENDPOINT + '/signup';
  public static LOGIN_ENDPOINT = ApiEndPoints.API_MAIN_ENDPOINT + '/login';
  public static CATEGORY_ENDPOINT = ApiEndPoints.BASE_ENDPOINT + 'category';
  public static EVENTS_ENDPOINT = ApiEndPoints.BASE_ENDPOINT + 'events';
  public static FAMOUS_VISITING_PLACE_ENDPOINT = ApiEndPoints.BASE_ENDPOINT + 'famousvisitingplace';
  public static FEEDBACK_ENDPOINT = ApiEndPoints.BASE_ENDPOINT + 'feedback';
  public static GUIDE_ENDPOINT = ApiEndPoints.BASE_ENDPOINT + 'guide';
  public static LOCAL_LOCATIONS_ENDPOINT = ApiEndPoints.BASE_ENDPOINT + 'locallocations';
  public static LOCAL_PRODUCTS_ENDPOINT = ApiEndPoints.BASE_ENDPOINT + 'localproducts';
  public static OFFERS_ENDPOINT = ApiEndPoints.BASE_ENDPOINT + 'offers';
  public static OWNER_ENDPOINT = ApiEndPoints.BASE_ENDPOINT + 'owner';
  public static USER_ENDPOINT = ApiEndPoints.BASE_ENDPOINT + 'user';
  public static LOCATION_ENDPOINT = ApiEndPoints.BASE_ENDPOINT + 'location';
  public static CUSTOM_ENDPOINT = ApiEndPoints.BASE_ENDPOINT + 'custom';


    // all famous places endpoints
    public static FAMOUSPLACE_ALL_ENDPOINTS = ApiEndPoints.FAMOUS_VISITING_PLACE_ENDPOINT + '/all';


    // all local places endpoints
    public static LOCAL_LOCATIONS_ALL_ENDPOINTS = ApiEndPoints.LOCAL_LOCATIONS_ENDPOINT + '/all';


     //guide
     public static GUIDE_All_ENDPOINTS = ApiEndPoints.GUIDE_ENDPOINT + '/all';

      //local-products
    public static LOCAL_PRODUCTS_ALL_ENDPOINTS = ApiEndPoints.LOCAL_PRODUCTS_ENDPOINT + '/all';
}
