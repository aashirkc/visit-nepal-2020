import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ApiEndPoints } from 'src/app/shared/services/api-end-points';
import { Location } from './model/location';
@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor(private http:HttpClient) { }

  getHeaders() {
    const token = 'Bearer ' + localStorage.getItem('token');
    const headerData = new HttpHeaders({ 'content-Type': 'application/json', authorization: token });
    return headerData;
  }
  findAllLocation() {
    const locationUrl = ApiEndPoints.LOCATION_ENDPOINT;
    return this.http.get(locationUrl, { headers: this.getHeaders() })
  }
}
