import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Localproducts } from '../model/localproducts';
import { Observable } from 'rxjs';
import { LocalproductsService } from '../localproducts.service';
import { Local } from 'protractor/built/driverProviders';

@Component({
  selector: 'app-localproductsdetails',
  templateUrl: './localproductsdetails.component.html',
  styleUrls: ['./localproductsdetails.component.scss']
})
export class LocalproductsdetailsComponent implements OnInit {
paramid:any;

currentProduct: Localproducts = new Localproducts;

product: Localproducts[];

  constructor(
    private route:ActivatedRoute,
    private localProductser: LocalproductsService) { }

  ngOnInit() {
    this.paramid = this.route.snapshot.paramMap.get('id');
    this.findProductById();
    this.getAllProduct();
}
 findProductById(){
   this.localProductser.findLocalProductById(this.paramid).subscribe(data=>{
     this.currentProduct = data as Localproducts;
   },error=>{
     console.log(error);
   });
 }

 getAllProduct(){
   this.localProductser.findAllLocalProducts().subscribe(data=>{
     this.product = data as Localproducts[];
   },error=>{
     console.log(error);
   });
 }

}
