import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocalproductsdetailsComponent } from './localproductsdetails.component';

describe('LocalproductsdetailsComponent', () => {
  let component: LocalproductsdetailsComponent;
  let fixture: ComponentFixture<LocalproductsdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocalproductsdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalproductsdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
