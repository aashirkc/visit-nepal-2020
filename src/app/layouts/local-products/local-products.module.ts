import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LocalProductsRoutingModule } from './local-products-routing.module';
import { LocalproductsComponent } from './localproducts.component';
import { LocalproductsdetailsComponent } from './localproductsdetails/localproductsdetails.component';
import { FormsModule } from '@angular/forms';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  declarations: [LocalproductsComponent, LocalproductsdetailsComponent],
  imports: [
    CommonModule,
    LocalProductsRoutingModule,
    NgxPaginationModule,
    FilterPipeModule,
    FormsModule
  ]
})
export class LocalProductsModule { }
