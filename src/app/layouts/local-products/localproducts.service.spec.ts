import { TestBed } from '@angular/core/testing';

import { LocalproductsService } from './localproducts.service';

describe('LocalproductsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LocalproductsService = TestBed.get(LocalproductsService);
    expect(service).toBeTruthy();
  });
});
