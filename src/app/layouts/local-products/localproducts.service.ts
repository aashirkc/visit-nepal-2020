import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { ApiEndPoints } from 'src/app/shared/services/api-end-points';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocalproductsService {

  constructor( private http:HttpClient) { }
  getHeader(){
    const token = 'Bearer ' + localStorage.getItem('token');
    const headersData = new HttpHeaders({'content-Type':'application/json', authorization:token});
    return headersData;
  }
  findAllLocalProducts():Observable<any>{
    const localProductUrl = ApiEndPoints.LOCAL_PRODUCTS_ALL_ENDPOINTS;
    return this.http.get<any>(localProductUrl, {headers:this.getHeader()});
  }
  findLocalProductById(id:number){
    const localProductUrl = ApiEndPoints.LOCAL_PRODUCTS_ENDPOINT + '/' +id;
    return this.http.get(localProductUrl, {headers:this.getHeader()});
  }
}
