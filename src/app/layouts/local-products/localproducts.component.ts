import { Component, OnInit } from '@angular/core';
import { Localproducts } from './model/localproducts';
import { Observable } from 'rxjs';
import { LocalproductsService } from './localproducts.service';

@Component({
  selector: 'app-localproducts',
  templateUrl: './localproducts.component.html',
  styleUrls: ['./localproducts.component.scss']
})
export class LocalproductsComponent implements OnInit {

  constructor(private localProductSer: LocalproductsService){ }

  currentProduct :Observable<Localproducts[]>;

  term : any = { name: '' };

  ngOnInit() {
    this.getAllLocalProducts();
  }
  getAllLocalProducts(){
    this.currentProduct = this.localProductSer.findAllLocalProducts();
  }

}
