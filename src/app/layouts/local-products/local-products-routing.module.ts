import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LocalproductsComponent } from './localproducts.component';
import { LocalproductsdetailsComponent } from './localproductsdetails/localproductsdetails.component';

const routes: Routes = [
  { path: "", component:LocalproductsComponent},
  { path: "detail/:id", component:LocalproductsdetailsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LocalProductsRoutingModule { }
