import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BlogdetailComponent } from './blogdetail/blogdetail.component';
import { BlogComponent } from './blog.component';
import { BlogRoutingModule } from './blog-routing.module';

@NgModule({
  declarations: [BlogdetailComponent, BlogComponent],
  imports: [
    CommonModule,
    BlogRoutingModule
  ]
})
export class BlogModule { }
