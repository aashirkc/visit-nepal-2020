import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HotelService {

  constructor(private http: HttpClient) { }

  findHotels() {
    const url = 'http://api.makcorps.com/free/mumbai';
    this.http.get(url, { headers: this.getHeader() });
  }

  private getHeader() {
    const accessToken = 'JWT ' + localStorage.getItem('access_token');
    const headerData = new HttpHeaders({ Authorization: accessToken });
    return headerData;
  }
}
