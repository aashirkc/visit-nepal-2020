import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CarsRoutingModule } from './cars-routing.module';
import { CarsComponent } from './cars.component';
import {DetailCarSidebarComponent} from './detail-car-sidebar/detail-car-sidebar.component';
@NgModule({
  declarations: [CarsComponent, DetailCarSidebarComponent],
  imports: [
    CommonModule,
    CarsRoutingModule
  ]
})
export class CarsModule { }
