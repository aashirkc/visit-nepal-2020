import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailCarSidebarComponent } from './detail-car-sidebar.component';

describe('DetailCarSidebarComponent', () => {
  let component: DetailCarSidebarComponent;
  let fixture: ComponentFixture<DetailCarSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailCarSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailCarSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
