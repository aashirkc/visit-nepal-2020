import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FamousVisitingPlaceDetailsComponent } from './famous-visiting-place-details.component';

describe('FamousVisitingPlaceDetailsComponent', () => {
  let component: FamousVisitingPlaceDetailsComponent;
  let fixture: ComponentFixture<FamousVisitingPlaceDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FamousVisitingPlaceDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FamousVisitingPlaceDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
