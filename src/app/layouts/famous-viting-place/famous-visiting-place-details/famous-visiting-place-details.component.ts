import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FamousVisitingPlaceServiceService } from '../famous-visiting-place-service.service';
import { LocalVisitingPlace, FamousVisitingPlace } from '../model/famous-visiting-place';

@Component({
  selector: 'app-famous-visiting-place-details',
  templateUrl: './famous-visiting-place-details.component.html',
  styleUrls: ['./famous-visiting-place-details.component.scss']
})
export class FamousVisitingPlaceDetailsComponent implements OnInit {

  public paramid: any;


 currentPlace: FamousVisitingPlace=new FamousVisitingPlace();

 places: FamousVisitingPlace[];


  constructor(
    private router: Router,
    private famousPlaceSer: FamousVisitingPlaceServiceService,
    private route: ActivatedRoute
    ) { }




  ngOnInit() {
    this.paramid = this.route.snapshot.paramMap.get('id');
    this.setPathOnUrl();
    this.getAllPlaces();
  }


  setPathOnUrl(){
    if(this.router.url.match('localdetail')){
     this.famousPlaceSer.findLocalLocationById(this.paramid).subscribe(data=>{
      this.currentPlace  = data as LocalVisitingPlace;
     });
    }
    else if(this.router.url.match('famousdetail')){
      this.famousPlaceSer.findFamousPlaceById(this.paramid).subscribe(data=>{
        this.currentPlace = data as FamousVisitingPlace;
      });
    }
  }




  getAllPlaces(){
    if(this.router.url.match('localdetail')){
      this.famousPlaceSer.findAllLocalPlaces().subscribe(data=>{
        this.places = data as LocalVisitingPlace[];
        console.log ('from place');
      },error=>{
        console.log(error);
      });
    }
    else if(this.router.url.match('famousdetail')){
      this.famousPlaceSer.findAllFamousPlaces().subscribe(data=>{
        this.places = data as FamousVisitingPlace[];
      },error=>{
        console.log(error);
      });
    }
  }



}
