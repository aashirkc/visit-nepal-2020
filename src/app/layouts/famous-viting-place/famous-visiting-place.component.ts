import { Component, OnInit } from '@angular/core';
import { FamousVisitingPlaceServiceService } from './famous-visiting-place-service.service';
import { FamousVisitingPlace, LocalVisitingPlace } from './model/famous-visiting-place';
import { Observable } from 'rxjs';
import { Ng2SearchPipe } from 'ng2-search-filter';

@Component({
  selector: 'app-famous-visiting-place',
  templateUrl: './famous-visiting-place.component.html',
  styleUrls: ['./famous-visiting-place.component.scss']
})
export class FamousVisitingPlaceComponent implements OnInit {
//variable for slider
  itemsPerSlide = 3;
  singleSlideOffset = false;
  noWrap = false;


  slidesChangeMessage = '';

  public : FamousVisitingPlace[];

  public famousPlace: Observable<FamousVisitingPlace[]>;
  //helps in filter the search
  term : any = { name: '' };



  public localPlace: LocalVisitingPlace[];


  constructor(private famousPlaceSer: FamousVisitingPlaceServiceService) { }

  ngOnInit() {

    this.getAllPlaces();
    this.getAllLocalPlaces();

  }

  // getAllPlaces(){
  //   return this.famousPlaceSer.findAllFamousPlaces().subscribe(data=>{
  //     this.famousPlace = data as FamousVisitingPlace[];
  //   },error=>{
  //     console.log(error);
  //   });
  // }

  getAllPlaces(){
    this.famousPlace = this.famousPlaceSer.searchFamousPlace()
  }

  getAllLocalPlaces(){
    return this.famousPlaceSer.findAllLocalPlaces().subscribe(data=>{
      this.localPlace = data as LocalVisitingPlace[];
    },error=>{
      console.log(error);
    })
  }




}
