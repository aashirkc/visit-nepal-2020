import { TestBed } from '@angular/core/testing';

import { FamousVisitingPlaceServiceService } from './famous-visiting-place-service.service';

describe('FamousVisitingPlaceServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FamousVisitingPlaceServiceService = TestBed.get(FamousVisitingPlaceServiceService);
    expect(service).toBeTruthy();
  });
});
