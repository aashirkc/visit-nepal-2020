import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FamousVisitingPlaceComponent } from './famous-visiting-place.component';

describe('FamousVisitingPlaceComponent', () => {
  let component: FamousVisitingPlaceComponent;
  let fixture: ComponentFixture<FamousVisitingPlaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FamousVisitingPlaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FamousVisitingPlaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
