import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FamousVisitingPlaceComponent } from './famous-visiting-place.component';
import { FamousVisitingPlaceDetailsComponent } from './famous-visiting-place-details/famous-visiting-place-details.component';

const routes: Routes = [
  {path: "", component: FamousVisitingPlaceComponent},
  {path: "localdetail/:id", component: FamousVisitingPlaceDetailsComponent},
  {path: "famousdetail/:id", component: FamousVisitingPlaceDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FamousVitingPlaceRoutingModule { }
