import { Location } from 'src/app/shared/Location/model/location';

export class FamousVisitingPlace {

  public id: number;
  public name: string;
  public description: string;
  public photo: string;
  public locationId: Location;
}

export class LocalVisitingPlace{
   public id:number;
  public name:string;
  public description:string;
  public photo:string;
  public locationId:Location;
}

export class Json{
  public key:any;
  public value:any;
}
