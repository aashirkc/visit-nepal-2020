import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { ApiEndPoints } from 'src/app/shared/services/api-end-points';
import { Observable } from 'rxjs';
import { FamousVisitingPlace } from './model/famous-visiting-place';
@Injectable({
  providedIn: 'root'
})
export class FamousVisitingPlaceServiceService {

  constructor(private http: HttpClient) { }

getHeader() {
  const token = 'Bearer ' + localStorage.getItem('token');
  const headerData = new HttpHeaders({ 'content-Type': 'application/json', authorization: token });
  return headerData;
}

findAllFamousPlaces() {
  const famousPlaceUrl = ApiEndPoints.FAMOUSPLACE_ALL_ENDPOINTS;

  return this.http.get(famousPlaceUrl, { headers: this.getHeader() });
}

findAllLocalPlaces(){
  const localPlaceUrl = ApiEndPoints.LOCAL_LOCATIONS_ALL_ENDPOINTS;
  return this.http.get(localPlaceUrl, {headers: this.getHeader() });
}
findFamousPlaceById(id: number) {
  const famousPlaceUrl = ApiEndPoints.FAMOUS_VISITING_PLACE_ENDPOINT + '/' + id;
  return this.http.get(famousPlaceUrl, { headers: this.getHeader() });
}

findLocalLocationById(id:number){
  const localLocationUrl = ApiEndPoints.LOCAL_LOCATIONS_ENDPOINT + '/' +id;
  return this.http.get(localLocationUrl, {headers:this.getHeader()});
}

searchFamousPlace(): Observable<FamousVisitingPlace[]>{
  const searchUrl = ApiEndPoints.FAMOUSPLACE_ALL_ENDPOINTS;
  return this.http.get<FamousVisitingPlace[]>(searchUrl,{headers:this.getHeader()});
}
}
