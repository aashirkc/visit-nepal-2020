import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FamousVitingPlaceRoutingModule } from './famous-visiting-place-routing.module';
import { FamousVisitingPlaceComponent } from './famous-visiting-place.component';
import { FamousVisitingPlaceDetailsComponent } from './famous-visiting-place-details/famous-visiting-place-details.component';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { FormsModule } from '@angular/forms';
import {NgxPaginationModule} from 'ngx-pagination';

import { FilterPipeModule } from 'ngx-filter-pipe';


@NgModule({
  declarations: [FamousVisitingPlaceComponent,FamousVisitingPlaceDetailsComponent],
  imports: [
    CommonModule,

    NgxPaginationModule,
    FamousVitingPlaceRoutingModule,
    CarouselModule,
    FormsModule,
    FilterPipeModule
  ]
})
export class FamousVitingPlaceModule { }
