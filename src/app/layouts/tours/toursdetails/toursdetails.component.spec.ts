import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToursdetailsComponent } from './toursdetails.component';

describe('ToursdetailsComponent', () => {
  let component: ToursdetailsComponent;
  let fixture: ComponentFixture<ToursdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToursdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToursdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
