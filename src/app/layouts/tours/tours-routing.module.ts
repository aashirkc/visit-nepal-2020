import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { ToursComponent } from './tours.component';
import { ToursdetailsComponent } from './toursdetails/toursdetails.component';

const routes: Routes = [
  { path: '', component: ToursComponent },
  { path: 'detail', component: ToursdetailsComponent }
];
@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes),
  ],
  exports:
    [RouterModule]
})
export class ToursRoutingModule { }
