import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutsComponent } from './layouts.component';
const routes: Routes = [
  {
    path: '',
    component: LayoutsComponent,
    children: [
      { path: '', redirectTo: 'home', pathMatch: 'prefix' },
      { path: 'home', loadChildren: './home/home.module#HomeModule' },
      { path: 'hotel', loadChildren: './hotel/hotel.module#HotelModule' },
      { path: 'cars', loadChildren: './cars/cars.module#CarsModule' },
      { path: 'blog', loadChildren: './blog/blog.module#BlogModule' },
      { path: 'tours', loadChildren: './tours/tours.module#ToursModule' },
      { path: 'user', loadChildren: './user/user.module#UserModule' },
      { path: 'guide', loadChildren: './guide/guide.module#GuideModule' },
      {path: 'famousvisitingplace', loadChildren: './famous-viting-place/famous-visiting-place.module#FamousVitingPlaceModule'},
      {path: 'localproducts', loadChildren:'./local-products/local-products.module#LocalProductsModule'}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutsRoutingModule { }
