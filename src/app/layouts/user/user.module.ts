import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './user.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from './profile/profile.component';
import { BookingComponent } from './booking/booking.component';
import { WishlistComponent } from './wishlist/wishlist.component';
import { CardsComponent } from './cards/cards.component';

@NgModule({
  declarations: [UserComponent, DashboardComponent, ProfileComponent, BookingComponent, WishlistComponent, CardsComponent],
  imports: [
    CommonModule,
    UserRoutingModule
  ]
})
export class UserModule { }
