export class Guide {
  public id:number;
  public name:string;
  public contactNO:number;
  public email:string;
  public description:string;
  public address:string;
  public photo:string;
}

