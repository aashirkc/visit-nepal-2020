import { Component, OnInit } from '@angular/core';
import { Guide } from './model/guide.model';
import { GuideService } from './guide.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-guide',
  templateUrl: './guide.component.html',
  styleUrls: ['./guide.component.scss']
})
export class GuideComponent implements OnInit {
public:Guide[];

  public guides:Observable<any>;

  term : any = { name: '' };

  constructor(private guideSer: GuideService) { }

  ngOnInit() {
    this.getAllGuides();
  }


getAllGuides(){
 this.guides = this.guideSer.findAllGuide()
}


}
