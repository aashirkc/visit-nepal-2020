import { Component, OnInit } from '@angular/core';
import { Guide } from '../model/guide.model';
import { Router, ActivatedRoute } from '@angular/router';
import { GuideService } from '../guide.service';

@Component({
  selector: 'app-guide-detail',
  templateUrl: './guide-detail.component.html',
  styleUrls: ['./guide-detail.component.scss']
})
export class GuideDetailComponent implements OnInit {
  paramid:any;
currentGuide: Guide = new Guide();

guide: Guide[];


  constructor(
    private route: ActivatedRoute,
    private guideSer: GuideService) { }

  ngOnInit() {
    this.paramid = this.route.snapshot.paramMap.get('id');
    this.findGuideById();
    this.getAllGuide();

  }


findGuideById(){
  this.guideSer.findGuideById(this.paramid).subscribe(data=>{
    this.currentGuide = data as Guide;
  },error=>{
    console.log(error);
  });
}

getAllGuide(){
  this.guideSer.findAllGuide().subscribe(data=>{
    this.guide = data as Guide[];
  },error=>{
    console.log(error);
  });
}


}
