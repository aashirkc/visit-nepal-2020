import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GuideComponent } from './guide.component';
import { GuideDetailComponent } from './guide-detail/guide-detail.component';

const routes: Routes = [
  {path:"", component:GuideComponent},
  {path:"detail/:id", component:GuideDetailComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GuideRoutingModule { }
