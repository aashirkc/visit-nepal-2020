import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { ApiEndPoints } from 'src/app/shared/services/api-end-points';
import { Observable } from 'rxjs';
import { Guide } from './model/guide.model';

@Injectable({
  providedIn: 'root'
})
export class GuideService {

  constructor(private http: HttpClient) {}
  getHeader() {
    const token = 'Bearer ' + localStorage.getItem('token');
    const headerData = new HttpHeaders({ 'content-Type': 'application/json', authorization: token });
    return headerData;
  }
  findAllGuide(): Observable<Guide[]> {
    const guideUrl = ApiEndPoints.GUIDE_All_ENDPOINTS;
    return this.http.get<Guide[]>(guideUrl, { headers: this.getHeader() });
  }
  findGuideById(id: number) {
    const guideUrl = ApiEndPoints.GUIDE_ENDPOINT + '/' + id;
    return this.http.get(guideUrl, { headers: this.getHeader() });
  }
}
