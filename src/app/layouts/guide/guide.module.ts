import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GuideRoutingModule } from './guide-routing.module';
import { GuideComponent } from './guide.component';
import { GuideDetailComponent } from './guide-detail/guide-detail.component';
import { GuideService } from './guide.service';
import {NgxPaginationModule} from 'ngx-pagination';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [GuideComponent, GuideDetailComponent],
  imports: [
    CommonModule,
    GuideRoutingModule,
    NgxPaginationModule,
    FilterPipeModule,
    FormsModule
  ],
  providers: [GuideService]
})
export class GuideModule { }
